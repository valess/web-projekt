<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Models;
use Nette\Application\UI\Form;

final class LogInPresenter extends Nette\Application\UI\Presenter
{
    private $authenticator;

    public function __construct(Models\MyAuthenticator $authenticator)
    {
        $this->authenticator = $authenticator;
    }

    protected function createComponentSignInForm(): Form
    {
        $form = new Form;
        $form->addText('username', 'Uživatelské jméno:')
            ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }

    public function signInFormSucceeded(Form $form, \stdClass $data): void
    {
    try {
        $user = $this->getUser();
        $user->setAuthenticator($this->authenticator);
        $user->login($data->username, $data->password); 
        $this->redirect('Homepage:');
    } catch (Nette\Security\AuthenticationException $e) {
        $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
        bdump($e);
    }
    }


    public function loginSkrzAutentifikator(Form $form, \stdClass $data): void
    {
        $user = $this->getUser();
        $user->setAuthenticator($this->authenticator);
        $user->login($data->username, $data->password); 
        $this->redirect('Homepage:');

    }

    public function actionOut(): void
    {
    $this->getUser()->logout();
    $this->flashMessage('Odhlášení bylo úspěšné.');
    $this->redirect('Homepage:');
    }
}