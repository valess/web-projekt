<?php

namespace App\Presenters;

use App\Models;
use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{

    private $db;
    private $produkt;

    public function __construct(
        Models\dbManager $db
    )
    {
        $this->db = $db;
    }
    //Vždy se vykoná
    function renderDefault() {
        $this->ukazProdukty();
    }
    //Zobrazí všechny produkty z db
    function ukazProdukty(){
        $produkty = $this->db->zobrazPrispevkyDB();
        $this->template->produkty = $produkty;
    }

}