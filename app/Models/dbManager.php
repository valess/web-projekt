<?php
namespace App\Models;
use App\Forms;
use Nette;

final class dbManager{
    private $database; //díky ní mohu přistupovat k db
    public $password;

    public function __construct(
        Nette\Database\Explorer $database,
        Nette\Security\Passwords $passwords
    )
    {
        $this->database = $database;
        $this->password = $passwords;
    }

    //Metoda vybere a zobrazí data z db
    public function zobrazPrispevkyDB() {
        $produkt = $this->database->table('products')->select('*')->fetchAll();
        bdump($produkt);
        return $produkt;
    }

    //Metoda vloží data o produktu do db
    public function vlozProductDB($data) {
        bdump($data);
        $this->database->table('products')->insert([
            'name' => $data->name,
            'description' => $data->description,
            'price' => $data->price,
            'category' => $data->category,
            'image' => $data->image
        ]);
    }
    //Metoda vloží data o uzivateli do db
    public function vlozUzivateleDB($username, $password, $name, $surname, $role, $email) {
        //bdump($data);
        $this->database->table('users')->insert([
            'username' => $username,
            'password' => $this->password->hash($password),
            'name' => $name,
            'surname' => $surname,
            'role' => $role,
            'email' => $email
        ]);
        //$this->redirect('Homepage:');

    }
}