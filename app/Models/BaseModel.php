<?php

namespace App\Models;

use Nette;
use Nette\Database\Table\Selection;

/**
 * Výchozí model k práci s databází.
 */
 class BaseModel {

    private $dbexplorer;
    private $tableName;

	public function __construct(Nette\Database\Explorer $dbexplorer) {
		$this->dbexplorer = $dbexplorer;
        $this->tableName = $this->getTable();
	}

    /**
     * @return string: Jméno tabulky
     */
    public function getTable(): String{
        return "users";
    }

    /**
     * Metoda začne výběr dat z tabulky
     *
     * @return Selection: Select dotaz
     */
    public function Select() {
        return $this->dbexplorer->table($this->tableName);
    }

    /**
     * Metoda vloží data do tabulky
     *
     * @param $data: pole pro vložení
     */
    public function Insert($data) {
        $this->dbexplorer->table($this->tableName)->insert($data);
    }

    /**
     * Metoda aktualizuje data v tabulce
     *
     * @param int $id: id řádku
     * @param $data: pole dat
     */
    public function Update(int $id, $data) : void {
        $this->dbexplorer->table($this->tableName)->wherePrimary($id)->update($data);
    }

    /**
     * Metoda smaže řádek s id
     *
     * @param $id: id řádku
     */
    public function Delete($id) : void {
        $this->dbexplorer->table($this->tableName)->wherePrimary($id)->delete();
    }

}