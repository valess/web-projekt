<?php
namespace App\Models;

use Nette;
use Nette\Security\SimpleIdentity;

class MyAuthenticator implements Nette\Security\Authenticator
{
	private $database;
	private $passwords;

	public function __construct(
		Nette\Database\Explorer $database,
		Nette\Security\Passwords $passwords
	) {
		$this->database = $database;
		$this->passwords = $passwords;
	}
	//Funkce, díky které se může uživatel přihlásit
	public function authenticate(string $username, string $password): /*SimpleIdentity*/ Nette\Security\IIdentity
	{
		$row = $this->database->table('users')
			->where('username', $username)
			->fetch();
		bdump($row);
		$res = $this->passwords->hash($password);
		bdump($res);

		if (!$row) {
			throw new Nette\Security\AuthenticationException('User not found.');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Invalid password.');
		}

		return new SimpleIdentity(
			$row->idusers,
			$row->role, // nebo pole více rolí
			['name' => $row->username]
		);
	}
}